import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { UploadComponent } from './upload/upload.component';
import { UploadService } from './upload/upload.service'

@NgModule({
  declarations: [UploadComponent],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, MaterialModule
  ],
  exports: [
    CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, UploadComponent
  ],
  providers: [UploadService]
})
export class SharedModule { }
