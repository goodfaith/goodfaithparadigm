import { NgModule } from '@angular/core';
import { AuthModule } from '../auth/auth.module';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [],
  imports: [
    AuthModule,
    UserModule,
    LayoutModule,
    MaterialModule,
    FlexLayoutModule,
   
  ],
  exports: [
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [AuthService]
})
export class CoreModule { }
