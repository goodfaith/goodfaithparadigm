import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { UploadComponent } from './shared/upload/upload.component';

const routes: Routes = [
  { path: 'upload', component: UploadComponent,},
  { path: '', component: WelcomeComponent, data: { title: 'Welcome' } },
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: '',  loadChildren: () => AuthModule }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
